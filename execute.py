from LibreriaNotificaciones import Notificador

tipoNotificador = {
    0: "Correo",
    1: "Facebook",
    2: "SMS",
    3: "Empresarial",
}

print("0. Correo\n1. Facebook\n2. SMS\n3. Empresarial\n")

elegirTipoNotificador = input("Seleccione los tipos de notificación escribiendo su número asignado:")
notificadoresSeleccionados = {}
# Recorre todos los notificadores seleccionados, pide sus destinatarios y los agrega al notificador
for n in elegirTipoNotificador:
    destinatarios = input("Seleccione los destinatarios para "+ tipoNotificador.get(int(n)) + " (separelos con una coma): ")
    destinatarios = destinatarios.split(',')
    notificadoresSeleccionados.update({int(n): destinatarios})

notificador = Notificador(notificadoresSeleccionados)
mensaje = input("Esciba el mensaje a enviar: ")
notificador.enviar(mensaje) #Enviar el mensaje