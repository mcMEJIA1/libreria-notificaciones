from abc import ABCMeta, abstractmethod
from colorama import init, Fore, Back, Style

#Creamos la clase abstracta según el modelo llamada Mensaje

class Mensaje(metaclass=ABCMeta):
       def __init__(self, destinatarios):
        self.destinatarios = destinatarios

       @abstractmethod
       def enviar(self):
           pass

#Creamos las clases que realizan herencia de la clase Mensaje y que representan cada tipo de mensaje
class MensajeCorreo(Mensaje):

    def enviar(self, mensaje: str):
        print(Fore.LIGHTRED_EX+"Email enviado con el mensaje: {} Para usuario(s): ".format(mensaje), self.destinatarios)

class MensajeSMS(Mensaje):

    def enviar(self, mensaje: str):
        print(Fore.WHITE+"SMS enviado con el mensaje: {} Para usuario(s):".format(mensaje), self.destinatarios)

class MensajeFacebook(Mensaje):

    def enviar(self, mensaje: str):
        print(Fore.BLUE+"Mensaje de Facebook enviado con el contenido: {} Para usuario(s): ".format(mensaje), self.destinatarios)

class MensajeEmpresarial(Mensaje):

    def enviar(self, mensaje: str):
        print(Fore.GREEN+"Mensaje empresarial enviado con el contenido: {} Para usuario(s): ".format(mensaje), self.destinatarios )


class Notificador:
    def __init__(self, notificadores):
        self.notificadores = notificadores

    def enviar(self, mensaje: str):
        creador = CreadorNotificaciones()
        creador.crear_Notificadores(self.notificadores, mensaje)


class CreadorNotificaciones:
    
    def crear_Notificadores(self, notificadores: {}, mensaje: str):

        #Se recorren los tipo de mensaje a enviar
        for n in notificadores.keys():
            destinatarios = notificadores.get(n)
            if n == 0 :
                self.crearMensajeCorreo(mensaje, destinatarios)
            elif n == 1 :
                self.crearMensajeFacebook(mensaje, destinatarios)
            elif n == 2 :
                self.crearMensajeSMS(mensaje, destinatarios)
            elif n == 3 :
                self.crearMensajeEmpresarial(mensaje, destinatarios)

    #Si es solicitado un tipo de mensaje a enviar, estos metodos crean el mensaje y lo envian a los destinatarias seleccionados
    def crearMensajeCorreo(self, mensaje: str, destinatarios):
        mensajeCorreo = MensajeCorreo(destinatarios)
        mensajeCorreo.enviar(mensaje)
    
    def crearMensajeSMS(self, mensaje: str, destinatarios):
        mensajeSMS = MensajeSMS(destinatarios)
        mensajeSMS.enviar(mensaje)
    
    def crearMensajeFacebook(self, mensaje: str, destinatarios):
        mensajeFacebook = MensajeFacebook(destinatarios)
        mensajeFacebook.enviar(mensaje)

    def crearMensajeEmpresarial(self, mensaje: str, destinatarios):
        mensajeEmpresarial = MensajeEmpresarial(destinatarios)
        mensajeEmpresarial.enviar(mensaje)
